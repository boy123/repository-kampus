
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Jenis Penelitian <?php echo form_error('jenis_penelitian') ?></label>
            <input type="text" class="form-control" name="jenis_penelitian" id="jenis_penelitian" placeholder="Jenis Penelitian" value="<?php echo $jenis_penelitian; ?>" />
        </div>
	    <input type="hidden" name="id_jenis_penelitian" value="<?php echo $id_jenis_penelitian; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jenis_penelitian') ?>" class="btn btn-default">Cancel</a>
	</form>
   