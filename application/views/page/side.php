<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="image/user/<?php echo $this->session->userdata('foto'); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('nama'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <?php if ($this->session->userdata('level') == 'admin') { ?>
        <li><a href="app"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        
        <li><a href="fakultas"><i class="fa fa-clone"></i> <span>Data Fakultas</span></a></li>
        <li><a href="jurusan"><i class="fa fa-clone"></i> <span>Data Jurusan</span></a></li>
        <li><a href="kategori"><i class="fa fa-clone"></i> <span>Data Kategori</span></a></li>
        <li><a href="jenis_penelitian"><i class="fa fa-clone"></i> <span>Data Jenis Penelitian</span></a></li>
        <li><a href="tahun"><i class="fa fa-clone"></i> <span>Data Tahun</span></a></li>
        <li><a href="Repository"><i class="fa fa-send"></i> <span>Data Repository</span></a></li>
        
        
      
        <li><a href="a_user"><i class="fa fa-users"></i> <span>Manajemen User</span></a></li>
    

        <?php } elseif($this->session->userdata('level') == 'mahasiswa') {
          ?>
          <li><a href="app"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <li><a href="Repository"><i class="fa fa-send"></i> <span>Data Repository</span></a></li>
          <li><a href="App/biodata/mahasiswa?id=<?php echo $this->session->userdata('id_user') ?>"><i class="fa fa-user"></i> <span>Biodata Mahasiswa</span></a></li>
          <li><a href="Pendidikan"><i class="fa fa-history"></i> <span>Riwayat Pendidikan</span></a></li>
        <?php } elseif($this->session->userdata('level') == 'dosen') {
          ?>
          <li><a href="app"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <li><a href="Repository"><i class="fa fa-send"></i> <span>Data Repository</span></a></li>
          <li><a href="App/biodata/dosen?id=<?php echo $this->session->userdata('id_user') ?>"><i class="fa fa-user"></i> <span>Biodata Dosen</span></a></li>
          <li><a href="Pendidikan"><i class="fa fa-history"></i> <span>Riwayat Pendidikan</span></a></li>
          <li><a href="mengajar_dosen"><i class="fa fa-graduation-cap"></i> <span>Riwayat Mengajar</span></a></li>
          <?php
        } ?>
        <!-- <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Faqs</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Tentang Aplikasi</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>