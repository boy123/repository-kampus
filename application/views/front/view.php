<?php $this->load->view('front/header'); ?>
<div class="row">
    <div class="col-md-6">
        <form action="" method="GET">
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Judul</label>
                <div class="col-sm-10">
                    <input type="text" name="judul" class="form-control">
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Tahun</label>
                <div class="col-sm-4">
                    <input type="date" name="tgl1" class="form-control">
                </div>
                <div class="col-sm-2 align-text-center">
                    Sampai
                </div>
                <div class="col-sm-4">
                    <input type="date" name="tgl2" class="form-control">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Penulis</label>
                <div class="col-sm-10">
                    <input type="text" name="penulis" class="form-control">
                </div>
            </div>

            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Kategori</label>
                <div class="col-sm-6">
                    <select class="form-control" name="kategori">
                        <option value="">Pilih</option>
                        <?php foreach ($this->db->get('kategori')->result() as $rw): ?>
                        <option value="<?php echo $rw->kategori ?>"><?php echo $rw->kategori ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-sm-2"></div>
                <div class="col-sm-2">
                    <div class="d-grid gap-2">
                        <input type="submit" value="Cari" name="cari" class="btn btn-outline-primary">
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<hr>
<?php if (isset($_GET['cari'])): ?>


<div class="row">
    <div class="col-md-12">
        <table class="table">
            <?php 
      $where = "";
      
      
          $judul = $this->input->get('judul');
          $tgl1 = $this->input->get('tgl1');
          $tgl2 = $this->input->get('tgl2');
          $penulis = $this->input->get('penulis');
          $kategori = $this->input->get('kategori');

          if ($judul !='') {
              $where .= "AND judul like '%$judul%' ";
          }

          if ($tgl1 !='' and $tgl2 !='') {
              $where .= "AND created BETWEEN '$tgl1' and '$tgl2' ";
          }

          if ($penulis !='') {
              $where .= "AND nama_penulis like '%$penulis%'  ";
          }

          if ($kategori !='') {
              $where .= "AND kategori='$kategori'  ";
          }
          $sql = "SELECT * FROM repository where id_user!='' $where ORDER BY id_repository DESC";
          
      

      foreach ($this->db->query($sql)->result() as $key => $value): ?>


            <tr>
                <td>
                    <img src="image/doc.png" width="150">
                </td>
                <td>
                    <a href="web/detail_repository/<?php echo $value->id_repository ?>">
                        <h5><?php echo $value->judul ?></h5>
                        <p><?php echo $value->nama_penulis ?>, <?php echo $value->tahun ?></p>
                    </a>
                </td>
            </tr>

            <?php endforeach ?>
        </table>
    </div>
</div>

<?php endif ?>

<?php $this->load->view('front/footer'); ?>