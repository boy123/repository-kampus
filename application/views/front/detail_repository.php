<?php $this->load->view('front/header'); ?>
<?php 
$id_repository = $this->uri->segment(3);
$dt = $this->db->get('repository')->row();

 ?>
<div class="row">
    <div class="col-md-12">
        <center>
            <h3><?php echo $dt->judul ?></h3>
        </center>
        <hr>
        <?php echo $dt->deskripsi ?>
    </div>
</div>

<hr>
<div class="row">
    <div class="col-md-1">
        <a href="image/file/<?php echo $dt->file_upload ?>">Download</a>
    </div>
</div>
<div class="row">
  <div class="col-md-6">
    <table class="table">
        <tr>
            <td>Penulis</td>
            <td>:</td>
            <td><?php echo $dt->nama_penulis ?> | <a href="web/biodata/mahasiswa/detail?id=<?php echo $dt->id_user ?>">Lihat Profil</a></td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td>:</td>
            <td><?php echo $dt->tahun ?></td>
        </tr>
        <tr>
            <td>Kategori</td>
            <td>:</td>
            <td><?php echo $dt->kategori ?></td>
        </tr>
        <tr>
            <td>Jenis Penelitian</td>
            <td>:</td>
            <td><?php echo $dt->jenis_penelitian ?></td>
        </tr>
        <tr>
            <td>Dosen Pembimbing</td>
            <td>:</td>
            <td>
                <?php if ($dt->pembimbing1 !=''): ?>
                    <p>
                        <?php echo get_data('dosen','id_dosen',$dt->pembimbing1,'nama_lengkap') ?> | <a href="web/biodata/dosen/detail?id=<?php echo get_data('dosen','id_dosen',$dt->pembimbing1,'id_user') ?>">Lihat Profil</a>
                    </p>
                <?php endif ?>

                <?php if ($dt->pembimbing2 !=''): ?>
                    <p>
                        <?php echo get_data('dosen','id_dosen',$dt->pembimbing2,'nama_lengkap') ?> | <a href="web/biodata/dosen/detail?id=<?php echo get_data('dosen','id_dosen',$dt->pembimbing2,'id_user') ?>">Lihat Profil</a>
                    </p>
                <?php endif ?>
                
            </td>
        </tr>
    </table>
  </div>
</div>

<?php $this->load->view('front/footer'); ?>