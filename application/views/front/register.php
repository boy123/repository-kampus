<?php $this->load->view('front/header'); ?>
      <div class="row">
        <div class="col-md-6">
          <form action="" method="POST">
            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Nama Lengkap</label>
              <div class="col-sm-10">
                <input type="text" name="nama" class="form-control">
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Username</label>
              <div class="col-sm-10">
                <input type="text" name="username" class="form-control">
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="password" name="password" class="form-control">
              </div>
            </div>

            <div class="row mb-3">
              <label class="col-sm-2 col-form-label">Sebagai</label>
              <div class="col-sm-6">
                <select class="form-control" name="level">
                  <option value="mahasiswa">Mahasiswa</option>
                  <option value="dosen">Dosen</option>
                  
                </select>
              </div>
            </div>
            
            <div class="row mb-3">
              <div class="col-sm-2"></div>
              <div class="col-sm-2">
                <div class="d-grid gap-2">
                  <button type="submit" class="btn btn-outline-primary">Daftar</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
<?php $this->load->view('front/footer'); ?>