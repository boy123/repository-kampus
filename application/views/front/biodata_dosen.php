<?php 
$id_user = $this->input->get('id');
$dt = $this->db->get('dosen', ['id_user'=>$id_user])->row();

$read = "";
if ($disable) {
	$read = "disabled";
}
 ?>

<div class="row">
    <div class="col-md-3">
        <div class="img-responsive">
            <img src="image/user/<?php echo get_data('a_user','id_user',$id_user,'foto') ?>" style="width: 100%;">
            <form class="form-horizontal" action="app/ganti_foto/<?php echo $id_user ?>/dosen" method="POST"
                enctype="multipart/form-data">
                <?php if ($disable == false): ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="file" class="form-control" name="foto" <?php echo $read ?>>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-block">Upload</button>
                    </div>
                </div>
                <?php endif ?>

            </form>
        </div>
    </div>
    <div class="col-md-9">
        <form class="form-horizontal" action="" method="POST">

            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Lengkap</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama_lengkap" placeholder=""
                        value="<?php echo $dt->nama_lengkap; ?>" <?php echo $read ?>>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Jenis Kelamin</label>

                <div class="col-sm-10">
                    <select name="jenis_kelamin" class="form-control" <?php echo $read ?>>
                        <option value="<?php echo $dt->jenis_kelamin ?>"><?php echo $dt->jenis_kelamin ?></option>
                        <option value="Laki-laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>

                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal Lahir</label>

                <div class="col-sm-10">
                    <input type="date" class="form-control" name="tanggal_lahir" placeholder=""
                        value="<?php echo $dt->tanggal_lahir; ?>" <?php echo $read ?>>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Alamat</label>

                <div class="col-sm-10">
                    <textarea class="form-control" name="alamat"
                        <?php echo $read ?>><?php echo $dt->alamat; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">No Kontak</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" name="kontak" placeholder="No Kontak"
                        value="<?php echo $dt->kontak; ?>" <?php echo $read ?>>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Social Media</label>

                <div class="col-sm-10">
                    <textarea class="form-control" name="social_media"
                        <?php echo $read ?>><?php echo $dt->social_media; ?></textarea>
                </div>
            </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label">Nomor Induk</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="nomor_induk" placeholder="Nomor Induk"
                    value="<?php echo $dt->nomor_induk; ?>" <?php echo $read ?>>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Status</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Dosen" value="Dosen" <?php echo $read ?>>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Status Aktif</label>

            <div class="col-sm-10">
                <select name="status_aktif" class="form-control" <?php echo $read ?>>
                    <option value="<?php echo $dt->status_aktif ?>"><?php echo $dt->status_aktif ?></option>
                    <option value="Aktif">Aktif</option>
                    <option value="Cuti">Cuti</option>
                    <option value="Mengundurkan Diri">Mengundurkan Diri</option>

                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Status Ikatan Kerja</label>

            <div class="col-sm-10">
                <select name="status_ikatan_kerja" class="form-control" <?php echo $read ?>>
                    <option value="<?php echo $dt->status_ikatan_kerja ?>"><?php echo $dt->status_ikatan_kerja ?>
                    </option>
                    <option value="Dosen Tetap">Dosen Tetap</option>
                    <option value="Dosen Tidak Tetap">Dosen Tidak Tetap</option>

                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Jabatan Fungsional</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="jafung" placeholder="Jabatan"
                    value="<?php echo $dt->jafung ?>" <?php echo $read ?>>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Jurusan</label>

            <div class="col-sm-10">
                <select name="jurusan" class="form-control" <?php echo $read ?>>
                    <option value="<?php echo $dt->jurusan ?>"><?php echo $dt->jurusan ?></option>
                    <?php foreach ($this->db->get('jurusan')->result() as $key => $value): ?>
                    <option value="<?php echo $value->jurusan ?>"><?php echo $value->jurusan ?></option>
                    <?php endforeach ?>

                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Fakultas</label>

            <div class="col-sm-10">
                <select name="fakultas" class="form-control" <?php echo $read ?>>
                    <option value="<?php echo $dt->fakultas ?>"><?php echo $dt->fakultas ?></option>
                    <?php foreach ($this->db->get('fakultas')->result() as $key => $value): ?>
                    <option value="<?php echo $value->fakultas ?>"><?php echo $value->fakultas ?></option>
                    <?php endforeach ?>

                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Tahun Masuk</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="tahun_masuk" placeholder="2019"
                    value="<?php echo $dt->tahun_masuk ?>" <?php echo $read ?>>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Tahun Keluar</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name="tahun_keluar" placeholder="2021"
                    value="<?php echo $dt->tahun_keluar ?>" <?php echo $read ?>>
            </div>
        </div>

        

        </form>
    </div>

</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#home">Riwayat Pendidikan</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu1">Riwayat Mengajar</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#menu2">Penelitian</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div id="home" class="container tab-pane active"><br>
        <table class="table table-bordered table-striped">
            <thead class="aler alert-warning">
                <tr>
                    <th>No</th>
                    <th>Nama Institusi</th>
                    <th>Status Pendidikan</th>
                    <th>Jurusan</th>
                    <th>Lulusan Tahun</th>
                    <th>IPK</th>
                </tr>
            </thead>
            <tbody>
                <?php
                            $no =1;
                             foreach ($this->db->get_where('pendidikan', ['id_user'=>$dt->id_user])->result() as $key => $value): ?>


                <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $value->nama_institusi ?></td>
                    <td><?php echo $value->status_pendidikan ?></td>
                    <td><?php echo $value->jurusan ?></td>
                    <td><?php echo $value->lulus_tahun ?></td>
                    <td><?php echo $value->nilai ?></td>
                </tr>
                <?php $no++; endforeach ?>
            </tbody>
        </table>
    </div>
    <div id="menu1" class="container tab-pane fade"><br>
        <table class="table table-bordered table-striped">
                        <thead class="aler alert-warning">
                            <tr>
                                <th>No</th>
                                <th>Semester</th>
                                <th>Kode Matakuliah</th>
                                <th>Nama Matakuliah</th>
                                <th>Kode Kelas</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no =1;
                             foreach ($this->db->get_where('mengajar_dosen', ['id_user'=>$dt->id_user])->result() as $key => $value): ?>


                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $value->semester ?></td>
                                <td><?php echo $value->kode_mk ?></td>
                                <td><?php echo $value->nama_mk ?></td>
                                <td><?php echo $value->kode_kelas ?></td>
                            </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
    </div>
    <div id="menu2" class="container tab-pane fade"><br>
        <table class="table table-bordered table-striped">
            <thead class="aler alert-warning">
                <tr>
                    <th>No</th>
                    <th>Judul Penelitian</th>
                    <th>Kategori</th>
                    <th>Jenis Penelitian</th>
                    <th>Tahun</th>
                </tr>
            </thead>
            <tbody>
                <?php
                            $no =1;
                             foreach ($this->db->get_where('repository', ['id_user'=>$dt->id_user])->result() as $key => $value): ?>


                <tr>
                    <td><?php echo $no ?></td>
                    <td>
                        <a href="web/detail_repository/<?php echo $value->id_repository ?>">
                            <?php echo $value->judul ?>
                        </a>
                    </td>
                    <td><?php echo $value->kategori ?></td>
                    <td><?php echo $value->jenis_penelitian ?></td>
                    <td><?php echo $value->tahun ?></td>
                </tr>
                <?php $no++; endforeach ?>
            </tbody>
        </table>
    </div>

</div>
    </div>
</div>

