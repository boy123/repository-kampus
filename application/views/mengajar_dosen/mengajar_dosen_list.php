
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('mengajar_dosen/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px" id="example2">
            <thead>
            <tr>
                <th>No</th>
        <th>Semester</th>
		<th>Kode Mk</th>
		<th>Nama Mk</th>
		<th>Kode Kelas</th>
		<th>Action</th>
            </tr>
            </thead>
            <tbody><?php
            $start = 1;
            if ($this->session->userdata('level')!='admin') {
                $this->db->where('id_user', $this->session->userdata('id_user'));
            }
            $mengajar_dosen_data = $this->db->get('mengajar_dosen');
            foreach ($mengajar_dosen_data->result() as $mengajar_dosen)
            {
                ?>
                <tr>
			<td width="80px"><?php echo $start ?></td>
            <td><?php echo $mengajar_dosen->semester ?></td>
			<td><?php echo $mengajar_dosen->kode_mk ?></td>
			<td><?php echo $mengajar_dosen->nama_mk ?></td>
			<td><?php echo $mengajar_dosen->kode_kelas ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('mengajar_dosen/update/'.$mengajar_dosen->id_mengajar),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('mengajar_dosen/delete/'.$mengajar_dosen->id_mengajar),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
                $start++;
            }
            ?>
            </tbody>
        </table>
        </div>
        
    