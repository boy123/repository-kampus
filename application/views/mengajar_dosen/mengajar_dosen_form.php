
        <form action="<?php echo $action; ?>" method="post">
	    <input type="hidden" name="id_user" value="<?php echo $this->session->userdata('id_user'); ?>">
	    <div class="form-group">
            <label for="varchar">Semester <?php echo form_error('semester') ?></label>
            <input type="text" class="form-control" name="semester" id="semester" placeholder="Ganjil 2016" value="<?php echo $semester; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Kode Mk <?php echo form_error('kode_mk') ?></label>
            <input type="text" class="form-control" name="kode_mk" id="kode_mk" placeholder="Kode Mk" value="<?php echo $kode_mk; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Nama Mk <?php echo form_error('nama_mk') ?></label>
            <input type="text" class="form-control" name="nama_mk" id="nama_mk" placeholder="Nama Mk" value="<?php echo $nama_mk; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Kode Kelas <?php echo form_error('kode_kelas') ?></label>
            <input type="text" class="form-control" name="kode_kelas" id="kode_kelas" placeholder="Kode Kelas" value="<?php echo $kode_kelas; ?>" />
        </div>
	    <input type="hidden" name="id_mengajar" value="<?php echo $id_mengajar; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('mengajar_dosen') ?>" class="btn btn-default">Cancel</a>
	</form>
   