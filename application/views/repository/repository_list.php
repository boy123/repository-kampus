
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('repository/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo (($this->session->userdata('message') != '') AND (preg_match("/swal/i", $this->session->userdata('message')) == false ) ) ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered table-striped" style="margin-bottom: 10px" id="example2">
            <thead>
            <tr>
                <th>No</th>
		<th>Nama Penulis</th>
		<th>Tahun</th>
		<th>Kategori</th>
		<th>Jenis Penelitian</th>
		<th>Judul</th>
		<th>Pembimbing1</th>
		<th>Pembimbing2</th>
		<th>File Upload</th>
		<th>Action</th>
            </tr>
            </thead>
            <tbody><?php
            $start =1;
            if ($this->session->userdata('level') != 'admin') {
                $this->db->where('id_user', $this->session->userdata('id_user'));
            }
            
            $repository_data = $this->db->get('repository');
            foreach ($repository_data->result() as $repository)
            {
                ?>
                <tr>
			<td width="80px"><?php echo $start ?></td>
			<td><?php echo $repository->nama_penulis ?></td>
			<td><?php echo $repository->tahun ?></td>
			<td><?php echo $repository->kategori ?></td>
			<td><?php echo $repository->jenis_penelitian ?></td>
			<td><?php echo $repository->judul ?></td>
			<td><?php echo get_data('dosen','id_dosen',$repository->pembimbing1,'nama_lengkap') ?></td>
			<td><?php echo get_data('dosen','id_dosen',$repository->pembimbing2,'nama_lengkap') ?></td>
			<td>
                <a href="image/file/<?php echo $repository->file_upload ?>"><?php echo $repository->file_upload ?></a>         
            </td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('repository/update/'.$repository->id_repository),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('repository/delete/'.$repository->id_repository),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
                $start++;
            }
            ?>
            </tbody>
        </table>
        </div>
        
    