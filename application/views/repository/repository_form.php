
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <input type="hidden" name="id_user" value="<?php echo $this->session->userdata('id_user'); ?>">
	    <div class="form-group">
            <label for="varchar">Nama Penulis <?php echo form_error('nama_penulis') ?></label>
            <input type="text" class="form-control" name="nama_penulis" id="nama_penulis" placeholder="Nama Penulis" value="<?php echo ($nama_penulis == '') ? $this->session->userdata('nama') : $nama_penulis; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Tahun <?php echo form_error('tahun') ?></label>
            <select name="tahun" class="form-control select2">
                <option value="<?php echo $tahun ?>"><?php echo $tahun ?></option>
                <?php
                $this->db->order_by('tahun', 'desc');
                 foreach ($this->db->get('tahun')->result() as $key => $value): ?>
                    <option value="<?php echo $value->tahun ?>"><?php echo $value->tahun ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Kategori <?php echo form_error('id_kategori') ?></label>
            <select name="id_kategori" class="form-control">
                <option value="<?php echo $id_kategori ?>"><?php echo get_data('kategori','id_kategori',$id_kategori,'kategori') ?></option>
                <?php foreach ($this->db->get('kategori')->result() as $key => $value): ?>
                    <option value="<?php echo $value->id_kategori ?>"><?php echo $value->kategori ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Jenis Penelitian <?php echo form_error('id_jenis_penelitian') ?></label>
            <select name="id_jenis_penelitian" class="form-control">
                <option value="<?php echo $id_jenis_penelitian ?>"><?php echo get_data('jenis_penelitian','id_jenis_penelitian',$id_jenis_penelitian,'jenis_penelitian') ?></option>
                <?php foreach ($this->db->get('jenis_penelitian')->result() as $key => $value): ?>
                    <option value="<?php echo $value->id_jenis_penelitian ?>"><?php echo $value->jenis_penelitian ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Judul <?php echo form_error('judul') ?></label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
        </div>
	    <div class="form-group">
            <label for="deskripsi">Deskripsi <?php echo form_error('deskripsi') ?></label>
            <textarea class="form-control editor" rows="3" name="deskripsi" id="deskripsi" placeholder="Deskripsi"><?php echo $deskripsi; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Pembimbing1 <?php echo form_error('pembimbing1') ?></label>
            <select name="pembimbing1" class="form-control select2">
                <option value="<?php echo $pembimbing1 ?>"><?php echo get_data('dosen','id_dosen',$pembimbing1,'nama_lengkap') ?></option>
                <?php foreach ($this->db->get('dosen')->result() as $key => $value): ?>
                    <option value="<?php echo $value->id_dosen ?>"><?php echo $value->nama_lengkap ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Pembimbing2 <?php echo form_error('pembimbing2') ?></label>
            <select name="pembimbing2" class="form-control select2">
                <option value="<?php echo $pembimbing2 ?>"><?php echo get_data('dosen','id_dosen',$pembimbing2,'nama_lengkap') ?></option>
                <?php foreach ($this->db->get('dosen')->result() as $key => $value): ?>
                    <option value="<?php echo $value->id_dosen ?>"><?php echo $value->nama_lengkap ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">File Upload <?php echo form_error('file_upload') ?></label>
            <input type="file" class="form-control" name="file_upload" id="file_upload" placeholder="File Upload" value="<?php echo $file_upload; ?>" />
            <input type="hidden" name="file_old" value="<?php echo $file_upload ?>">
            <div>
                <?php if ($file_upload != ''): ?>
                    <b>*) File Sebelumnya : </b><br>
                    <a href="image/file/<?php echo $file_upload ?>"><?php echo $file_upload ?></a>
                <?php endif ?>
            </div>
        </div>
	    <input type="hidden" name="id_repository" value="<?php echo $id_repository; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('repository') ?>" class="btn btn-default">Cancel</a>
	</form>
   