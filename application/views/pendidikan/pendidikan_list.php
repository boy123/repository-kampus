
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo $retVal = ($this->session->userdata('level') != 'admin') ? anchor(site_url('pendidikan/create'),'Create', 'class="btn btn-primary"') : ''; ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px" id="example2">
            <thead>
            <tr>
                <th>No</th>
		<th>Nama Institusi</th>
		<th>Status Pendidikan</th>
		<th>Jurusan</th>
		<th>Lulus Tahun</th>
		<th>Nilai</th>
		<th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $start = 1;
            if ($this->session->userdata('level') != 'admin') {
                $this->db->where('id_user', $this->session->userdata('id_user'));
            }
            $pendidikan_data = $this->db->get('pendidikan');
            foreach ($pendidikan_data->result() as $pendidikan)
            {
                ?>
                <tr>
			<td width="80px"><?php echo $start ?></td>
			<td><?php echo $pendidikan->nama_institusi ?></td>
			<td><?php echo $pendidikan->status_pendidikan ?></td>
			<td><?php echo $pendidikan->jurusan ?></td>
			<td><?php echo $pendidikan->lulus_tahun ?></td>
			<td><?php echo $pendidikan->nilai ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('pendidikan/update/'.$pendidikan->id_pendidikan),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('pendidikan/delete/'.$pendidikan->id_pendidikan),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>

                <?php
                $start++;
            }
            ?>
            </tbody>
        </table>
        </div>
        
    