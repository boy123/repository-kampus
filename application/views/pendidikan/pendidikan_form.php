
        <form action="<?php echo $action; ?>" method="post">
	    <input type="hidden" name="id_user" value="<?php echo $this->session->userdata('id_user') ?>">
	    <div class="form-group">
            <label for="varchar">Nama Institusi <?php echo form_error('nama_institusi') ?></label>
            <input type="text" class="form-control" name="nama_institusi" id="nama_institusi" placeholder="Nama Institusi" value="<?php echo $nama_institusi; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Status Pendidikan <?php echo form_error('status_pendidikan') ?></label>
            <input type="text" class="form-control" name="status_pendidikan" id="status_pendidikan" placeholder="Status Pendidikan" value="<?php echo $status_pendidikan; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Jurusan <?php echo form_error('jurusan') ?></label>
            <input type="text" class="form-control" name="jurusan" id="jurusan" placeholder="Jurusan" value="<?php echo $jurusan; ?>" />
        </div>
	    <div class="form-group">
            <label for="year">Lulus Tahun <?php echo form_error('lulus_tahun') ?></label>
            <input type="text" class="form-control" name="lulus_tahun" id="lulus_tahun" placeholder="Lulus Tahun" value="<?php echo $lulus_tahun; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Nilai <?php echo form_error('nilai') ?></label>
            <input type="text" class="form-control" name="nilai" id="nilai" placeholder="Nilai" value="<?php echo $nilai; ?>" />
        </div>
	    <input type="hidden" name="id_pendidikan" value="<?php echo $id_pendidikan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pendidikan') ?>" class="btn btn-default">Cancel</a>
	</form>
   