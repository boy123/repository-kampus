
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Fakultas <?php echo form_error('fakultas') ?></label>
            <input type="text" class="form-control" name="fakultas" id="fakultas" placeholder="Fakultas" value="<?php echo $fakultas; ?>" />
        </div>
	    <input type="hidden" name="id_fakultas" value="<?php echo $id_fakultas; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('fakultas') ?>" class="btn btn-default">Cancel</a>
	</form>
   