<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pendidikan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pendidikan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pendidikan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pendidikan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pendidikan/index.html';
            $config['first_url'] = base_url() . 'pendidikan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pendidikan_model->total_rows($q);
        $pendidikan = $this->Pendidikan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pendidikan_data' => $pendidikan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Riwayat Pendidikan',
            'konten' => 'pendidikan/pendidikan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Pendidikan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_pendidikan' => $row->id_pendidikan,
		'id_user' => $row->id_user,
		'nama_institusi' => $row->nama_institusi,
		'status_pendidikan' => $row->status_pendidikan,
		'jurusan' => $row->jurusan,
		'lulus_tahun' => $row->lulus_tahun,
		'nilai' => $row->nilai,
	    );
            $this->load->view('pendidikan/pendidikan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendidikan'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Riwayat Pendidikan',
            'konten' => 'pendidikan/pendidikan_form',
            'button' => 'Create',
            'action' => site_url('pendidikan/create_action'),
	    'id_pendidikan' => set_value('id_pendidikan'),
	    'id_user' => set_value('id_user'),
	    'nama_institusi' => set_value('nama_institusi'),
	    'status_pendidikan' => set_value('status_pendidikan'),
	    'jurusan' => set_value('jurusan'),
	    'lulus_tahun' => set_value('lulus_tahun'),
	    'nilai' => set_value('nilai'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'nama_institusi' => $this->input->post('nama_institusi',TRUE),
		'status_pendidikan' => $this->input->post('status_pendidikan',TRUE),
		'jurusan' => $this->input->post('jurusan',TRUE),
		'lulus_tahun' => $this->input->post('lulus_tahun',TRUE),
		'nilai' => $this->input->post('nilai',TRUE),
	    );

            $this->Pendidikan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pendidikan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pendidikan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Riwayat Pendidikan',
                'konten' => 'pendidikan/pendidikan_form',
                'button' => 'Update',
                'action' => site_url('pendidikan/update_action'),
		'id_pendidikan' => set_value('id_pendidikan', $row->id_pendidikan),
		'id_user' => set_value('id_user', $row->id_user),
		'nama_institusi' => set_value('nama_institusi', $row->nama_institusi),
		'status_pendidikan' => set_value('status_pendidikan', $row->status_pendidikan),
		'jurusan' => set_value('jurusan', $row->jurusan),
		'lulus_tahun' => set_value('lulus_tahun', $row->lulus_tahun),
		'nilai' => set_value('nilai', $row->nilai),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendidikan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pendidikan', TRUE));
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'nama_institusi' => $this->input->post('nama_institusi',TRUE),
		'status_pendidikan' => $this->input->post('status_pendidikan',TRUE),
		'jurusan' => $this->input->post('jurusan',TRUE),
		'lulus_tahun' => $this->input->post('lulus_tahun',TRUE),
		'nilai' => $this->input->post('nilai',TRUE),
	    );

            $this->Pendidikan_model->update($this->input->post('id_pendidikan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pendidikan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pendidikan_model->get_by_id($id);

        if ($row) {
            $this->Pendidikan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pendidikan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pendidikan'));
        }
    }

    public function _rules() 
    {
	// $this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('nama_institusi', 'nama institusi', 'trim|required');
	$this->form_validation->set_rules('status_pendidikan', 'status pendidikan', 'trim|required');
	$this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required');
	$this->form_validation->set_rules('lulus_tahun', 'lulus tahun', 'trim|required');
	$this->form_validation->set_rules('nilai', 'nilai', 'trim|required');

	$this->form_validation->set_rules('id_pendidikan', 'id_pendidikan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pendidikan.php */
/* Location: ./application/controllers/Pendidikan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-10-01 16:09:49 */
/* https://jualkoding.com */