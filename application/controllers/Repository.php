<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Repository extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Repository_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'repository/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'repository/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'repository/index.html';
            $config['first_url'] = base_url() . 'repository/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Repository_model->total_rows($q);
        $repository = $this->Repository_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'repository_data' => $repository,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Repository',
            'konten' => 'repository/repository_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Repository_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_repository' => $row->id_repository,
		'id_user' => $row->id_user,
		'nama_penulis' => $row->nama_penulis,
		'tahun' => $row->tahun,
		'id_kategori' => $row->id_kategori,
		'kategori' => $row->kategori,
		'id_jenis_penelitian' => $row->id_jenis_penelitian,
		'jenis_penelitian' => $row->jenis_penelitian,
		'judul' => $row->judul,
		'deskripsi' => $row->deskripsi,
		'pembimbing1' => $row->pembimbing1,
		'pembimbing2' => $row->pembimbing2,
		'file_upload' => $row->file_upload,
	    );
            $this->load->view('repository/repository_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('repository'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Data Repository',
            'konten' => 'repository/repository_form',
            'button' => 'Create',
            'action' => site_url('repository/create_action'),
	    'id_repository' => set_value('id_repository'),
	    'id_user' => set_value('id_user'),
	    'nama_penulis' => set_value('nama_penulis'),
	    'tahun' => set_value('tahun'),
	    'id_kategori' => set_value('id_kategori'),
	    'kategori' => set_value('kategori'),
	    'id_jenis_penelitian' => set_value('id_jenis_penelitian'),
	    'jenis_penelitian' => set_value('jenis_penelitian'),
	    'judul' => set_value('judul'),
	    'deskripsi' => set_value('deskripsi'),
	    'pembimbing1' => set_value('pembimbing1'),
	    'pembimbing2' => set_value('pembimbing2'),
	    'file_upload' => set_value('file_upload'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $file_upload = upload_gambar_biasa('file', 'image/file/', 'pdf', 10000, 'file_upload');
            if (preg_match("/<p>/i", $file_upload)) {
                $this->session->set_flashdata('message', alert_biasa(strip_tags($file_upload),"error"));
                redirect('repository');
            }

            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'nama_penulis' => $this->input->post('nama_penulis',TRUE),
		'tahun' => $this->input->post('tahun',TRUE),
		'id_kategori' => $this->input->post('id_kategori',TRUE),
		'kategori' => get_data('kategori','id_kategori',$this->input->post('id_kategori'),'kategori'),
		'id_jenis_penelitian' => $this->input->post('id_jenis_penelitian',TRUE),
		'jenis_penelitian' => get_data('jenis_penelitian','id_jenis_penelitian',$this->input->post('id_jenis_penelitian'),'jenis_penelitian'),
		'judul' => $this->input->post('judul',TRUE),
		'deskripsi' => $this->input->post('deskripsi',TRUE),
		'pembimbing1' => $this->input->post('pembimbing1',TRUE),
		'pembimbing2' => $this->input->post('pembimbing2',TRUE),
		'file_upload' => $file_upload,
	    );

            $this->Repository_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('repository'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Repository_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Data Repository',
                'konten' => 'repository/repository_form',
                'button' => 'Update',
                'action' => site_url('repository/update_action'),
		'id_repository' => set_value('id_repository', $row->id_repository),
		'id_user' => set_value('id_user', $row->id_user),
		'nama_penulis' => set_value('nama_penulis', $row->nama_penulis),
		'tahun' => set_value('tahun', $row->tahun),
		'id_kategori' => set_value('id_kategori', $row->id_kategori),
		'kategori' => set_value('kategori', $row->kategori),
		'id_jenis_penelitian' => set_value('id_jenis_penelitian', $row->id_jenis_penelitian),
		'jenis_penelitian' => set_value('jenis_penelitian', $row->jenis_penelitian),
		'judul' => set_value('judul', $row->judul),
		'deskripsi' => set_value('deskripsi', $row->deskripsi),
		'pembimbing1' => set_value('pembimbing1', $row->pembimbing1),
		'pembimbing2' => set_value('pembimbing2', $row->pembimbing2),
		'file_upload' => set_value('file_upload', $row->file_upload),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('repository'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_repository', TRUE));
        } else {

            $file_upload = upload_gambar_biasa('file', 'image/file/', 'pdf', 10000, 'file_upload');
            if (preg_match("/<p>/i", $file_upload)) {
                $this->session->set_flashdata('message', alert_biasa(strip_tags($file_upload),"error"));
                redirect('repository');
            }

            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
		'nama_penulis' => $this->input->post('nama_penulis',TRUE),
		'tahun' => $this->input->post('tahun',TRUE),
		'id_kategori' => $this->input->post('id_kategori',TRUE),
		'kategori' => $this->input->post('kategori',TRUE),
		'id_jenis_penelitian' => $this->input->post('id_jenis_penelitian',TRUE),
		'jenis_penelitian' => $this->input->post('jenis_penelitian',TRUE),
		'judul' => $this->input->post('judul',TRUE),
		'deskripsi' => $this->input->post('deskripsi',TRUE),
		'pembimbing1' => $this->input->post('pembimbing1',TRUE),
		'pembimbing2' => $this->input->post('pembimbing2',TRUE),
		'file_upload' => $retVal = ($_FILES['file_upload']['name'] !='') ? $file_upload : $this->input->post('file_old') ,
	    );

            $this->Repository_model->update($this->input->post('id_repository', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('repository'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Repository_model->get_by_id($id);

        if ($row) {
            $this->Repository_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('repository'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('repository'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('nama_penulis', 'nama penulis', 'trim|required');
	$this->form_validation->set_rules('tahun', 'tahun', 'trim|required');
	$this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required');
	$this->form_validation->set_rules('id_jenis_penelitian', 'id jenis penelitian', 'trim|required');
	$this->form_validation->set_rules('judul', 'judul', 'trim|required');
	$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

	$this->form_validation->set_rules('id_repository', 'id_repository', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Repository.php */
/* Location: ./application/controllers/Repository.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-10-02 09:15:37 */
/* https://jualkoding.com */