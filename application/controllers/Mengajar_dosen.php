<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mengajar_dosen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mengajar_dosen_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'mengajar_dosen/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'mengajar_dosen/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'mengajar_dosen/index.html';
            $config['first_url'] = base_url() . 'mengajar_dosen/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Mengajar_dosen_model->total_rows($q);
        $mengajar_dosen = $this->Mengajar_dosen_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'mengajar_dosen_data' => $mengajar_dosen,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Riwayat Mengajar',
            'konten' => 'mengajar_dosen/mengajar_dosen_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Mengajar_dosen_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_mengajar' => $row->id_mengajar,
		'id_user' => $row->id_user,
		'kode_mk' => $row->kode_mk,
		'nama_mk' => $row->nama_mk,
		'kode_kelas' => $row->kode_kelas,
	    );
            $this->load->view('mengajar_dosen/mengajar_dosen_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mengajar_dosen'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Riwayat Mengajar',
            'konten' => 'mengajar_dosen/mengajar_dosen_form',
            'button' => 'Create',
            'action' => site_url('mengajar_dosen/create_action'),
	    'id_mengajar' => set_value('id_mengajar'),
	    'id_user' => set_value('id_user'),
        'semester' => set_value('semester'),
	    'kode_mk' => set_value('kode_mk'),
	    'nama_mk' => set_value('nama_mk'),
	    'kode_kelas' => set_value('kode_kelas'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
        'semester' => $this->input->post('semester',TRUE),
		'kode_mk' => $this->input->post('kode_mk',TRUE),
		'nama_mk' => $this->input->post('nama_mk',TRUE),
		'kode_kelas' => $this->input->post('kode_kelas',TRUE),
	    );

            $this->Mengajar_dosen_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('mengajar_dosen'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mengajar_dosen_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Riwayat Mengajar',
                'konten' => 'mengajar_dosen/mengajar_dosen_form',
                'button' => 'Update',
                'action' => site_url('mengajar_dosen/update_action'),
		'id_mengajar' => set_value('id_mengajar', $row->id_mengajar),
		'id_user' => set_value('id_user', $row->id_user),
        'semester' => set_value('semester', $row->semester),
		'kode_mk' => set_value('kode_mk', $row->kode_mk),
		'nama_mk' => set_value('nama_mk', $row->nama_mk),
		'kode_kelas' => set_value('kode_kelas', $row->kode_kelas),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mengajar_dosen'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_mengajar', TRUE));
        } else {
            $data = array(
		'id_user' => $this->input->post('id_user',TRUE),
        'semester' => $this->input->post('semester',TRUE),
		'kode_mk' => $this->input->post('kode_mk',TRUE),
		'nama_mk' => $this->input->post('nama_mk',TRUE),
		'kode_kelas' => $this->input->post('kode_kelas',TRUE),
	    );

            $this->Mengajar_dosen_model->update($this->input->post('id_mengajar', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('mengajar_dosen'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mengajar_dosen_model->get_by_id($id);

        if ($row) {
            $this->Mengajar_dosen_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('mengajar_dosen'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mengajar_dosen'));
        }
    }

    public function _rules() 
    {
	// $this->form_validation->set_rules('id_user', 'id user', 'trim|required');
    $this->form_validation->set_rules('semester', 'Semester', 'trim|required');
	$this->form_validation->set_rules('kode_mk', 'kode mk', 'trim|required');
	$this->form_validation->set_rules('nama_mk', 'nama mk', 'trim|required');
	$this->form_validation->set_rules('kode_kelas', 'kode kelas', 'trim|required');

	$this->form_validation->set_rules('id_mengajar', 'id_mengajar', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Mengajar_dosen.php */
/* Location: ./application/controllers/Mengajar_dosen.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-10-02 04:19:47 */
/* https://jualkoding.com */