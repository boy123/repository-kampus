<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller {

	public $image = '';
	
	public function index()
	{
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
		$data = array(
			'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
    }

    public function pengembangan()
    {
        $this->session->set_flashdata('message', alert_biasa('Sedang dalam pengembangan','info'));
        redirect("app");
    }

    public function biodata($level,$detail='')
    {
    	if ($_POST) {
    		if ($level == 'mahasiswa') {
    			$this->db->where('id_user', $this->input->get('id'));
    			$this->db->update('mahasiswa', $_POST);
    			$this->session->set_flashdata('message', alert_biasa('Biodata berhasil diubah','success'));
        		redirect("app/biodata/".$level."?id=".$this->input->get('id'));
    		} else {
    			$this->db->where('id_user', $this->input->get('id'));
    			$this->db->update('dosen', $_POST);
    			$this->session->set_flashdata('message', alert_biasa('Biodata berhasil diubah','success'));
        		redirect("app/biodata/".$level."?id=".$this->input->get('id'));
    		}
    	} else {
    		if ($detail=='detail') {
    			$disable = true;
    		} else {
    			$disable = false;
    		}
    		if ($level == 'mahasiswa') {
    			$data = array(
					'konten' => 'biodata/biodata_mahasiswa',
		            'judul_page' => 'Biodata Mahasiswa',
				);
				$data['disable'] = $disable;
				$this->load->view('v_index', $data);
    		} else {
    			$data = array(
					'konten' => 'biodata/biodata_dosen',
		            'judul_page' => 'Biodata Dosen',
				);
				$data['disable'] = $disable;
				$this->load->view('v_index', $data);
    		}
    	}
    }

    public function ganti_foto($id_user, $url)
    {
    	
    	if ($_FILES['foto']['name'] !='') {
    		$img = upload_gambar_biasa('user', 'image/user/', 'jpg|png|jpeg', 10000, 'foto');
    		$this->db->where('id_user', $id_user);
    		$this->db->update('a_user', ['foto'=>$img]);
    		$this->session->set_flashdata('message', alert_biasa('Foto berhasil diubah','success'));
        	redirect("app/biodata/".$url."?id=".$id_user);
    	}
    }

   
	

	

	
}
