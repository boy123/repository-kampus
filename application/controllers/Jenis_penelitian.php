<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_penelitian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Jenis_penelitian_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'jenis_penelitian/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jenis_penelitian/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'jenis_penelitian/index.html';
            $config['first_url'] = base_url() . 'jenis_penelitian/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Jenis_penelitian_model->total_rows($q);
        $jenis_penelitian = $this->Jenis_penelitian_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'jenis_penelitian_data' => $jenis_penelitian,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Jenis Penelitian',
            'konten' => 'jenis_penelitian/jenis_penelitian_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Jenis_penelitian_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_jenis_penelitian' => $row->id_jenis_penelitian,
		'jenis_penelitian' => $row->jenis_penelitian,
	    );
            $this->load->view('jenis_penelitian/jenis_penelitian_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenis_penelitian'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Jenis Penelitian',
            'konten' => 'jenis_penelitian/jenis_penelitian_form',
            'button' => 'Create',
            'action' => site_url('jenis_penelitian/create_action'),
	    'id_jenis_penelitian' => set_value('id_jenis_penelitian'),
	    'jenis_penelitian' => set_value('jenis_penelitian'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'jenis_penelitian' => $this->input->post('jenis_penelitian',TRUE),
	    );

            $this->Jenis_penelitian_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('jenis_penelitian'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Jenis_penelitian_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Jenis Penelitian',
                'konten' => 'jenis_penelitian/jenis_penelitian_form',
                'button' => 'Update',
                'action' => site_url('jenis_penelitian/update_action'),
		'id_jenis_penelitian' => set_value('id_jenis_penelitian', $row->id_jenis_penelitian),
		'jenis_penelitian' => set_value('jenis_penelitian', $row->jenis_penelitian),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenis_penelitian'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_jenis_penelitian', TRUE));
        } else {
            $data = array(
		'jenis_penelitian' => $this->input->post('jenis_penelitian',TRUE),
	    );

            $this->Jenis_penelitian_model->update($this->input->post('id_jenis_penelitian', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jenis_penelitian'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Jenis_penelitian_model->get_by_id($id);

        if ($row) {
            $this->Jenis_penelitian_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jenis_penelitian'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jenis_penelitian'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('jenis_penelitian', 'jenis penelitian', 'trim|required');

	$this->form_validation->set_rules('id_jenis_penelitian', 'id_jenis_penelitian', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Jenis_penelitian.php */
/* Location: ./application/controllers/Jenis_penelitian.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-09-30 19:58:22 */
/* https://jualkoding.com */