<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function index()
	{
		$this->load->view('front/view');
	}

	public function detail_repository($id)
	{
		$this->load->view('front/detail_repository');
	}

	public function register()
	{
		if ($_POST) {
			$this->db->insert('a_user', [
				'nama_lengkap' => $this->input->post('nama'),
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'level' => $this->input->post('level')
			]);
			$id_user = $this->db->insert_id();
			if ($this->input->post('level') == 'mahasiswa') {
				$this->db->insert('mahasiswa', ['id_user'=>$id_user,'nama_lengkap'=>$this->input->post('nama')]);
			} else {
				$this->db->insert('dosen', ['id_user'=>$id_user,'nama_lengkap'=>$this->input->post('nama')]);
			}
			
			$this->session->set_flashdata('message', alert_biasa('Pendaftaran kamu berhasil, silahkan login','success'));
			redirect('login','refresh');
		} else {
			$this->load->view('front/register');
		}
	}

	public function biodata($level,$detail='')
    {
    	if ($detail=='detail') {
    			$disable = true;
    		} else {
    			$disable = false;
    		}
    		if ($level == 'mahasiswa') {
    			$data = array(
					'konten' => 'front/biodata_mahasiswa',
		            'judul_page' => 'Biodata Mahasiswa',
				);
				$data['disable'] = $disable;
				$this->load->view('front/biodata', $data);
    		} else {
    			$data = array(
					'konten' => 'front/biodata_dosen',
		            'judul_page' => 'Biodata Dosen',
				);
				$data['disable'] = $disable;
				$this->load->view('front/biodata', $data);
    		}
    }

}

/* End of file Web.php */
/* Location: ./application/controllers/Web.php */